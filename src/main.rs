#[macro_use]
extern crate nom;
extern crate chrono;
extern crate encoding;
extern crate winapi;
extern crate kernel32;

mod rcs_types;
mod rcs_parser;
mod intern;
mod input;

use std::io::{self, Read, Error};
use std::fs::File;
use rcs_parser::Parser;
use std::path::Path;
use std::result::Result;
use input::{wide_from_console_string, paths_from_wide};

use nom::*;

fn get_content(path: ::std::ffi::OsString) -> Result<Vec<u8>, String> {
    let mut file = try!(File::open(path).map_err(|e| e.to_string()));
    let mut data = Vec::new();
    try!(file.read_to_end(&mut data).map_err(|e| e.to_string()));
    Ok(data)
}

fn do_it() -> Result<(), String> {
    let mut input = Vec::new();
    let empty = &b""[..];
    try!(io::stdin().read_to_end(&mut input).map_err(|e| e.to_string()));
    let pathlist = wide_from_console_string(&input[..]);
    let paths = paths_from_wide(&pathlist[..]);
    // let mut bufs = Vec::new();
    // let mut rcs_files = Vec::new();
    let mut p = Parser::new();
    // for path in paths {
    // let mut data = try!(get_content(path));
    // let (mut x, r) = p.rcstext(&data);
    // p = x;
    // if let IResult::Done(x, rcs) = r {
    // rcs_files.push(rcs);
    // }
    // FIXME should only need to do this if we keep rcs
    // bufs.push(data);
    // }
    //
    Ok(())
}

fn main() {
    match do_it() {
        Ok(_) => println!("Parsed everything!"),
        Err(e) => println!("Err: {:?}", e),
    }
}
