//! Types for data parsed from RCS files
//! Names based on those from the RCS grammar
// FIXME: RCS is an 8 bit format.
//        Is there anything we actually need to translate to a UTF string for
//        or should we just ship &[u8] everywhere?
use encoding::all::ISO_8859_1;
use encoding::{Encoding, DecoderTrap};
use std::fmt;
use std::convert::From;
use std::default::Default;
use std::ops::Deref;

/// rcs num production
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct Number(pub Vec<u16>);

/// RCS is an 8 bit format. generally iso-8859-1
/// tokens in the grammar only use the lower 7 bits
/// We only ever decode into a string for informative purposes.
/// Output data is written as is, and will preserve the
/// original encoding
#[derive(PartialEq, Eq, Hash, Clone)]
pub struct RcsString<'a>(pub &'a[u8]);

/// Assume iso-8859-1 if debugging
impl <'a> fmt::Debug for RcsString<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", ISO_8859_1.decode(self.0, DecoderTrap::Ignore))
    }
}
impl <'a> RcsString<'a> {
    pub fn from_bytes(bytes: &'a[u8]) -> Self {
        RcsString(bytes)
    }
}

impl <'a> Deref for RcsString<'a> {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        &self.0
    }
}
/// rcs num converted to a condensed date
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct Timestamp(pub u32);

/// a log message
/// Is it worth newtyping each different usage?
#[allow(dead_code)]
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct Log<'a>(pub RcsString<'a>);

#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct Symbol<'a> {
    pub name: RcsString<'a>, //&'a str,
    pub number: Number
}

#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct Delta<'a> {
    // FIXME make separate types instead of str?
    // pros - shouldn't be comparing them
    // cons? - we're going to want to atomize most if not all strings
    //       - regardless of what they represent
    pub author: RcsString<'a>,
    pub state: Option<RcsString<'a>> ,
    pub commitid: Option<RcsString<'a>>,
    pub branches: Vec<Number>,
    pub number: Number,
    pub date: Timestamp,
    pub next: Option<Number>,
    pub dead: bool
}
// not currently used
#[allow(dead_code)]
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct FileText<'a> {
    //FIXME use path from standard library?
    //FIXME why keep filename here instead of at the top struct?
    pub filename: &'a str,
    pub length: u64,
    pub offset: u64
}

// deltatext production, except we keep text as a reference to the backing file
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct DeltaText<'a> {
    pub number: Number,
    pub log: RcsString<'a>,
    // FIXME? original code uses a reference to file locaiton
    //        instead of refering to the content
    // The text of the delta
    pub text: RcsString<'a>
    //pub text: FileText<'a>
}

#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub enum ExpandMode {
    KeywordValue,	// default form, $<key>: <value>$
    KeywordValueLocker,	// like KeywordValue but with locker's name inserted
    Keyword,	        // keyword-only expansion, $<key>$
    Value,	        // value-only expansion, $<value>$
    Old,	        // old-value expansion
    OldNoEol,	        // old-value with no EOL normalization
    Unspecified,	// Not specified on command line
}

impl <'a>From<&'a[u8]> for ExpandMode {
    fn from(input: &'a[u8]) -> Self {
        match input {
            b"kv" => ExpandMode::KeywordValue,
            b"kvl" => ExpandMode::KeywordValueLocker,
            b"k" => ExpandMode::Keyword,
            b"v" => ExpandMode::Value,
            b"o" => ExpandMode::Old,
            b"b" => ExpandMode::OldNoEol,
            _ => ExpandMode::default()
        }
    }
}

impl Default for ExpandMode {
    fn default() -> Self {
        ExpandMode::Unspecified
    }
}

/// rcstext production
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct File<'a> {
    // FIXME use library for file path
    pub export_name: &'a str,
    // FIXME can we infer this if not supplied?
    // -- can we get files with no versions at all?
    pub head: Option<Number>,
    pub branch: Option<Number>,
    pub symbols: Vec<Symbol<'a>>,
    pub desc: RcsString<'a>,
    pub comment: Option<RcsString<'a>>,
    pub skew_vulnerable: Timestamp,
    pub expand_mode: ExpandMode,
    pub deltas: Vec<Delta<'a>>,
    pub delta_texts: Vec<DeltaText<'a>>
}
