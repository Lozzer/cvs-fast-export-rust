use std::collections::HashSet;
use std::hash::Hash;

struct Interner<T> {
    items: HashSet<T>
}

impl <T:Eq + Hash + Clone> Interner<T> {
    fn new() -> Self {
        Interner {
            items: HashSet::new()
        }
    }
    fn intern(&mut self, item: T) -> T {
        if let Some(i) = self.items.get(&item) {
            return i.clone();
        }
        self.items.insert(item.clone());
        item
    }
}

#[cfg(test)]
mod test {
    use super::Interner;
    use std::vec::Vec;
    use std::rc::Rc;
   #[test]
    fn test_interner() {
        let mut interner: Interner<Rc<Vec<u16>>> = Interner::new();
        let c1 = Rc::new(vec![1, 2]);
        let c2 = interner.intern(c1);
        let c3 = Rc::new(vec![1, 2]);
        let c4 = interner.intern(c3);
        assert_eq!(c2, c4);
    }
}
