// TODO Unix version
// reading is much easier as we get a C string.
// decide whether to split on \0 or \n or both

extern crate kernel32;
extern crate winapi;

use std::io::{self, Read};
use std::ptr;
use std::fs::metadata;
use std::ffi::OsString;
use std::os::windows::ffi::OsStringExt;
use std::i32;

/// Convert windows console input to wide string that can
/// be used by OS functions
pub fn wide_from_console_string(bytes: &[u8]) -> Vec<u16> {
    assert!(bytes.len() < i32::MAX as usize);
    let mut wide;
    let mut len;
    unsafe {
        let cp = kernel32::GetOEMCP();
        len = kernel32::MultiByteToWideChar(cp,
                                            0,
                                            bytes.as_ptr() as *const i8,
                                            bytes.len() as i32,
                                            ptr::null_mut(),
                                            0);
        wide = Vec::with_capacity(len as usize);
        len = kernel32::MultiByteToWideChar(cp,
                                            0,
                                            bytes.as_ptr() as *const i8,
                                            bytes.len() as i32,
                                            wide.as_mut_ptr(),
                                            len);
        wide.set_len(len as usize);
    }
    wide
}

/// Extract paths from a list supplied as Cr LF
/// separated wide string
/// Would use a generic split on substring if it existed
// FIXME could maybe use nom for this?
// FIXME tests
pub fn paths_from_wide(wide: &[u16]) -> Vec<OsString> {
    let mut r = Vec::new();
    let mut start = 0;
    let mut i = start;
    let len = wide.len() - 1;
    while i < len {
        if wide[i] == 13 && wide[i + 1] == 10 {
            if i > start {
                r.push(OsString::from_wide(&wide[start..i]));
            }
            start = i + 2;
            i = i + 2;
        } else {
            i = i + 1;
        }
    }
    if i > start {
        r.push(OsString::from_wide(&wide[start..i]));
    }
    r
}
