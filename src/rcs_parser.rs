//! Parse RCS (Revision Control System) Files
//! See grammar below, albeit some versions of CVS extend this
//! https://www.gnu.org/software/rcs/manual/html_node/File-format.html#File-format
//! function names map on to the productions in this grammar

//FIXME use nom 2.0's whitespace handling
use std::str;
use std::u32;
use nom::*;
use rcs_types::*;
use chrono::NaiveDate;

#[derive(Debug)]
pub struct Parser {
}

impl Parser {
    pub fn new() -> Self {
        Parser {}
    }
}

const RCS_EPOCH: i64 = 378691200;     // 1982-01-01T00:00:00
const RCS_OMEGA: i64 = u32::MAX as i64;      // 2118-02-07T06:28:15

/// Convert an RCS date to a timestamp
// FIXME explain why (reduced data size etc)
// FIXME make method on Timestamp type?
fn rcs_timestamp(d: Vec<u16>) -> Timestamp {
    assert!(d.len() == 6,
            "CVS date should be six numbers separated with dots. e.g. 2001.01.01.04.30.21");
    let y = if d[0] <= 1900 { d[0] + 1900 } else { d[0] } as i32;
    // FIXME unchecked_get?
    // FIXME UTC rather than Naive?
    let date = NaiveDate::from_ymd(y, d[1] as u32, d[2] as u32)
        .and_hms(d[3] as u32, d[4] as u32, d[5] as u32);
    let ts = date.timestamp();
    if ts < RCS_EPOCH {
        panic!("RCS Date {:?} is before epoch", d);
    }
    if ts - RCS_EPOCH > RCS_OMEGA {
        panic!("RCS Date {:?} too far in future", d);
    }
    return Timestamp((ts - RCS_EPOCH) as u32);
}

// convert sequence of digits encoded in ascii to u16 value
fn u16_from_digits(digits: &[u8]) -> u16 {
    digits.iter().fold(0, |prod, x| prod * 10 + (x - b'0') as u16)
}

/// Parse a sequence of ASCII encoded digits into a u16
named!(u16digit<u16>,
       do_parse!(
           digits: take_while1!(is_digit) >>
           (u16_from_digits(digits))
       )
);

/// Parse numbers separated by dots as used by RCS revision numbers
/// e.g. 1.2.3.4.
named!(dotted_numbers<Vec<u16> >,
       separated_nonempty_list!(char!('.'), u16digit)
);

named!(num<Number>,
       do_parse!(
           cn: dotted_numbers >>
           (Number(cn))
       )
);

fn is_visible(c: u8) -> bool {
    (c >= 33 && c <= 126) || c >= 160
}

fn is_idchar(c: u8) -> bool {
    is_visible(c) && c != b'$' && c != b',' && c != b'.' && c != b':' && c != b';' && c != b'@'
}

fn is_idchar_or_dot(c: u8) -> bool {
    is_visible(c) && c != b'$' && c != b',' && c != b':' && c != b';' && c != b'@'
}

fn is_whitespace(c: u8) -> bool {
    (c >= 8 && c <= 13) || c == 32
}

named!(skip, take_while!(is_whitespace));

/// Read an escaped RCS string, leave escaped
/// RCS string is delimited by @
/// Literal @ is escaped as @@
fn string<'a>(input: &'a [u8]) -> IResult<&'a [u8], RcsString> {
    let len = input.len();
    if len < 1 {
        return IResult::Incomplete(Needed::Unknown);
    }
    if input[0] != b'@' {
        // FIXME error message should be something like
        // RCS string should be delimited by @
        return IResult::Error(ErrorKind::Custom(0));
    }
    // index to current char (RCS is 8 bit encoding)
    let mut i = 1;
    while i < len {
        // FIXME get_unchecked or iter
        // Check for end delimiter
        if input[i] == b'@' {
            // if there's another @ then it is an escape sequence
            if i + 1 < len && input[i + 1] == b'@' {
                i += 1; // skip next char too
            } else {
                // end of string
                let result = RcsString(&input[1..i]);
                return IResult::Done(&input[i + 1..], result);
            }
        }
        i += 1;
    }
    IResult::Incomplete(Needed::Unknown)
}

named!(desc<RcsString>,
       do_parse!(
           tag!("desc") >>
           skip >>
           desc: string >> 
           (desc)
       )
);

named!(sym<RcsString>,
       map!(take_while1!(is_idchar), RcsString::from_bytes)
);

named!(id<RcsString>,
       map!(take_while1!(is_idchar_or_dot), RcsString::from_bytes)
);

impl Parser {
    method!(symbol<Parser, Symbol>, mut self,
       do_parse!(
           skip >>
           name: sym >>
           tag!(":") >>
           number: num >>
           (Symbol {
               name: name,
               number: number
           })
       )
    );

    method!(symbol_list<Parser, Vec<Symbol> >, mut self,
       do_parse!(
           tag!("symbols\n") >>
           symbol_list: separated_list!(char!('\n'), call_m!(self.symbol)) >>
           tag!(";") >>
           (symbol_list)
       )
    );

    method!(delta_text<Parser, DeltaText>, mut self,
           do_parse!(
               number: num >> skip >>
                   tag!("log") >> skip >>
                   log: string >> skip >>
                   tag!("text") >> skip >>
                   text: string >>
               (DeltaText {
                   number: number,
                   log: log,
                   text: text
               })
           )
    );

    method!(delta_text_list<Parser, Vec<DeltaText> >, mut self,
       many0!(do_parse!(
           delta_text: call_m!(self.delta_text) >>
           skip >>
           (delta_text)
       ))
    );

    method!(branch<Parser, Number>, mut self,
       do_parse!(
           number: num >>
           skip >>
           (number)
       )
    );

    method!(commitid<Parser, RcsString>, mut self,
       do_parse!(
           skip >>
           tag!("commitid") >>
           skip >>
           commitid: sym >>
           tag!(";") >>
           (commitid)
       )
    );

    method!(delta<Parser, Delta>, mut self,
       do_parse!(
           number: num >> skip >>
           tag!("date") >> skip >> date: dotted_numbers >> tag!(";") >> skip >>
           tag!("author") >> skip >> author: id >> tag!(";") >> skip >>
           tag!("state") >> skip >> state: opt!(id) >> tag!(";") >> skip >>
           tag!("branches") >> skip >> branches: many0!(call_m!(self.branch)) >> tag!(";") >> skip >>
           tag!("next") >> skip >> next: opt!(num) >> tag!(";") >>
           // note complete! - no longer a streaming parser
           commitid: opt!(complete!(call_m!(self.commitid))) >>
           ({
               let dead = state == Some(RcsString(b"dead"));
               Delta {
                   author: author,
                   state: state,
                   commitid: commitid,
                   branches: branches,
                   number: number,
                   date: rcs_timestamp(date),
                   next: next,
                   dead: dead
               }
           })
       )
    );

    method!(delta_list<Parser, Vec<Delta> >, mut self,
           many0!(do_parse!(
               delta: call_m!(self.delta) >>
                   skip >>
                   (delta)
           ))
    );

    method!(rcstext_branch<Parser, Option<Number> >, mut self,
           do_parse!(
               tag!("branch") >>
                   skip >>
                   branch: opt!(num) >>
                   skip >>
                   tag!(";") >>
                   (branch)
           )
    );

    /// from locks production, just consume input
    method!(id_num_list<Parser, ()>, mut self,
           fold_many0!(do_parse!(
               skip >> many1!(id) >> tag!(":") >> num >>
               (())
           ), (), |_, _| () )
    );

    method!(strict<Parser, ()>, mut self,
           do_parse!(
               tag!("strict") >>
                   skip >>
                   tag!(";") >>
               (())
           )
    );

    method!(comment<Parser, Option<RcsString> >, mut self,
           do_parse!(
               tag!("comment") >>
                   skip >>
                   comment: opt!(string) >>
                   skip >>
                   tag!(";") >>
                   (comment)
           )
    );

    method!(expand<Parser, Option<RcsString> >, mut self,
           do_parse!(
               tag!("expand") >>
                   skip >>
                   expand: opt!(string) >>
                   skip >>
                   tag!(";") >>
                   (expand)
           )
    );

    /// rcstext and admin productions in one
    method!(pub rcstext<Parser, File>, mut self,
            complete!(do_parse!(
                tag!("head") >> skip >> head: opt!(num) >> tag!(";") >> skip >>
                    branch: opt!(call_m!(self.rcstext_branch)) >> skip >>
                    tag!("access") >> skip >> many0!(id) >> tag!(";") >> skip >>
                    symbols: call_m!(self.symbol_list) >> skip >>
                    tag!("locks") >> call_m!(self.id_num_list) >> skip >> tag!(";") >> skip >>
                    opt!(call_m!(self.strict)) >> skip >>
                // FIXME? cvs-fast-export ignores integrity tag
                    comment: opt!(call_m!(self.comment)) >> skip >>
                    expand: opt!(call_m!(self.expand)) >> skip >>
                    deltas: call_m!(self.delta_list) >> skip >>
                    desc: desc >> skip >>
                    delta_texts: call_m!(self.delta_text_list) >>
                    (File {
                // FIXME export_name
                    export_name: "FIXME",
                    head: head,
                // branch is Option<Option<Number>>
                    branch: branch.unwrap_or(None),
                    symbols: symbols,
                    desc: desc,
                // comment is Option<Option<RcsString>>
                    comment: comment.unwrap_or(None),
                // FIXME skew_vulnerable
                    skew_vulnerable: Timestamp(0),
                // expand is Option<Option<RcsString>>
                // FIXME RcsString is already a reference to a slice
                // feels like ref x is an uncessary layer of indirection
                expand_mode: if let Some(Some(ref x)) = expand {
                    <ExpandMode as From<&[u8]>>::from(x)
                } else {
                    ExpandMode::default()
                    },
                    deltas: deltas,
                    delta_texts: delta_texts

                })
            ))
    );
}

#[cfg(test)]
mod test {
    //! public API tests
    use super::{rcs_timestamp, is_idchar, num, dotted_numbers, u16digit, string, desc, Parser};
    use rcs_types::*;
    use nom::*;

    #[test]
    fn test_rcs_timestamp1() {
        assert_eq!(rcs_timestamp(vec![82, 1, 1, 0, 0, 0]), Timestamp(0));
        assert_eq!(rcs_timestamp(vec![1982, 1, 1, 0, 0, 0]), Timestamp(0));
    }

    #[test]
    #[should_panic]
    fn test_rcs_timestamp_not_enough_parts() {
        assert_eq!(rcs_timestamp(vec![1, 2, 3]), Timestamp(0));
    }

    #[test]
    #[should_panic(expected = "RCS Date [81, 1, 1, 1, 1, 1] is before epoch")]
    fn test_rcs_timestamp_too_old() {
        assert_eq!(rcs_timestamp(vec![81, 1, 1, 1, 1, 1]), Timestamp(0));
    }

    #[test]
    #[should_panic(expected= "RCS Date [2119, 1, 1, 1, 1, 1] too far in future")]
    fn test_rcs_timestamp_too_new() {
        assert_eq!(rcs_timestamp(vec![2119, 1, 1, 1, 1, 1]), Timestamp(0));
    }

    #[test]
    fn test_u16digit_parser() {
        let empty = &b""[..];
        assert_eq!(u16digit(b"a"), IResult::Error(ErrorKind::TakeWhile1));
        assert_eq!(u16digit(b"123"), IResult::Done(empty, (123)));
        assert_eq!(u16digit(b"32767"), IResult::Done(empty, (32767)));
        assert_eq!(u16digit(b"65535"), IResult::Done(empty, (65535)));
    }

    #[test]
    fn test_dotted_numbers() {
        let empty = &b""[..];
        let v: Vec<u16> = vec![1, 1];
        assert_eq!(dotted_numbers(b"1.1"), IResult::Done(empty, v));
    }

    #[test]
    fn test_num() {
        let empty = &b""[..];
        let v: Vec<u16> = vec![1, 1];
        assert_eq!(num(b"1.1"), IResult::Done(empty, Number(v)));
    }

    #[test]
    fn test_is_idchar() {
        assert_eq!(is_idchar(b'a'), true);
        assert_eq!(is_idchar(b'.'), false);
    }

    #[test]
    fn test_symbol() {
        let empty = &b""[..];
        let p = Parser::new();
        let expected = Symbol {
            name: RcsString(b"EMACS_23_1_RC"),
            number: Number(vec![1, 4, 0, 2]),
        };
        let (p, result) = p.symbol(b"\tEMACS_23_1_RC:1.4.0.2");
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_symbol_list() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"symbols
\tsneakums-nasty-hack:1.2.0.2
\tbeaujolais:1.1.1.1
\tgar:1.1.1;";
        let expected = vec![
            Symbol{
                name: RcsString(b"sneakums-nasty-hack"),
                number: Number(vec![1, 2, 0, 2])
            },
            Symbol{
                name: RcsString(b"beaujolais"),
                number: Number(vec![1, 1, 1, 1])
            },
            Symbol{
                name: RcsString(b"gar"),
                number: Number(vec![1, 1, 1])
            }
        ];
        let (p, result) = p.symbol_list(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_string() {
        let empty = &b""[..];
        let input = b"@Simple String@";
        let expected = RcsString(b"Simple String");
        assert_eq!(string(input), IResult::Done(empty, expected));
        let input = b"@String Containing @@@";
        let expected = RcsString(b"String Containing @@");
        assert_eq!(string(input), IResult::Done(empty, expected));

        let input = b"@String Containing @@@ and more @@ stuff to parse@";
        let expected = RcsString(b"String Containing @@");
        let rest = &b" and more @@ stuff to parse@"[..];
        assert_eq!(string(input), IResult::Done(rest, expected));

        let input = b"@String with multiple @@ at @@ signs@";
        let expected = RcsString(b"String with multiple @@ at @@ signs");
        assert_eq!(string(input), IResult::Done(empty, expected));

    }

    #[test]
    fn test_desc() {
        let empty = &b""[..];
        let input = b"desc
@First File

See loz@@flower.powernet.co.uk@";
        let expected = RcsString(b"First File

See loz@@flower.powernet.co.uk");
        assert_eq!(desc(input), IResult::Done(empty, expected));
    }

    #[test]
    fn test_delta_text() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"1.1
log
@Initial revision
@
text
@d1 8
a8 1
To compile, simply type \"./configure\" and then \"make\".
@";
        let log = RcsString(b"Initial revision\n");
        let text =
            RcsString(b"d1 8\na8 1\nTo compile, simply type \"./configure\" and then \"make\".\n");
        let expected = DeltaText {
            number: Number(vec![1, 1]),
            log: log,
            text: text,
        };
        let (p, result) = p.delta_text(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_commitid() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"commitid   m2wTb5BJWcSozMLx;";
        let expected = RcsString(b"m2wTb5BJWcSozMLx");
        let (p, result) = p.commitid(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_delta_basic() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"1.2
date    2001.02.21.21.01.14;    author unclepedro;      state Exp;
branches;
next    1.1;";
        let expected = Delta {
            author: RcsString(b"unclepedro"),
            state: Some(RcsString(b"Exp")),
            commitid: None,
            branches: vec![],
            number: Number(vec![1, 2]),
            date: Timestamp(604098074),
            next: Some(Number(vec![1, 1])),
            dead: false,
        };
        let (p, result) = p.delta(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_delta_multiple_branches() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"1.11
date    2007.01.21.04.21.43;    author gm;      state Exp;
branches
        1.11.2.1
        1.11.4.1;
next    1.10;";
        let expected = Delta {
            author: RcsString(b"gm"),
            state: Some(RcsString(b"Exp")),
            commitid: None,
            branches: vec![
                Number(vec![1, 11, 2, 1]),
                Number(vec![1, 11, 4, 1])
            ],
            number: Number(vec![1, 11]),
            date: Timestamp(790662103),
            next: Some(Number(vec![1, 10])),
            dead: false,
        };
        let (p, result) = p.delta(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_delta_dead_with_commitid() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"1.4.6.3
date    2007.10.19.00.00.12;    author miles;   state dead;
branches;
next    ;
commitid        PE3046x0qvSRf8Cs;";
        let expected = Delta {
            author: RcsString(b"miles"),
            state: Some(RcsString(b"dead")),
            commitid: Some(RcsString(b"PE3046x0qvSRf8Cs")),
            branches: vec![],
            number: Number(vec![1, 4, 6, 3]),
            date: Timestamp(814060812),
            next: None,
            dead: true,
        };
        let (p, result) = p.delta(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_expand() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"expand @o@;";
        let expected = Some(RcsString(b"o"));
        let (p, result) = p.expand(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_comment() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"comment @w @;";
        let expected = Some(RcsString(b"w "));
        let (p, result) = p.comment(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_strict() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"strict ;";
        let expected = ();
        let (p, result) = p.strict(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn test_id_num_list() {
        let empty = &b""[..];
        let p = Parser::new();
        let input = b"joerg:1.1.1.1.2.1.2.1";
        let expected = ();
        let (p, result) = p.id_num_list(input);
        assert_eq!(result, IResult::Done(empty, expected));
    }

    #[test]
    fn rcstext_test() {
        let empty = &b""[..];
        let input = b"head	1.1;
branch	1.1.1;
access;
symbols
	sneakums-nasty-hack:1.1.1.1.0.2
	beaujolais:1.1.1.1
	gar:1.1.1;
locks; strict;
comment	@# @;


1.1
date	2000.11.27.19.57.55;	author monkeymaster;	state Exp;
branches
	1.1.1.1;
next	;

1.1.1.1
date	2000.11.27.19.57.55;	author monkeymaster;	state Exp;
branches;
next	;


desc
@@



1.1
log
@Initial revision
@
text
@timestamp
@


1.1.1.1
log
@FIND KITTEN
@
text
@@
";
        let desc = RcsString(b"");
        let comment = RcsString(b"# ");
        let log1 = RcsString(b"Initial revision\n");
        let text1 = RcsString(b"timestamp\n");
        let log2 = RcsString(b"FIND KITTEN\n");
        let text2 = RcsString(b"");
        let expected = File {
            export_name: "FIXME",
            head: Some(Number(vec![1, 1])),
            branch: Some(Number(vec![1, 1, 1])),
            symbols: vec![
                Symbol {
                    name: RcsString(b"sneakums-nasty-hack"),
                    number: Number(vec![1, 1, 1, 1, 0, 2])
                }, Symbol {
                    name: RcsString(b"beaujolais"),
                    number: Number(vec![1, 1, 1, 1])
                }, Symbol {
                    name: RcsString(b"gar"),
                    number: Number(vec![1, 1, 1])
                }
            ],
            desc: desc,
            comment: Some(comment),
            skew_vulnerable: Timestamp(0),
            expand_mode: ExpandMode::Unspecified,
            deltas: vec![
                Delta {
                    author: RcsString(b"monkeymaster"),
                    state: Some(RcsString(b"Exp")),
                    commitid: None,
                    branches: vec![Number(vec![1, 1, 1, 1])],
                    number: Number(vec![1, 1]),
                    date: Timestamp(596663875),
                    next: None,
                    dead: false
                },
                Delta {
                    author: RcsString(b"monkeymaster"),
                    state: Some(RcsString(b"Exp")),
                    commitid: None,
                    branches: vec![],
                    number: Number(vec![1, 1, 1, 1]),
                    date: Timestamp(596663875),
                    next: None,
                    dead: false
                }
            ],
            delta_texts: vec![
                DeltaText {
                    number: Number(vec![1, 1]),
                    log: log1,
                    text: text1
                }, DeltaText {
                    number: Number(vec![1, 1, 1, 1]),
                    log: log2,
                    text: text2
                }
            ],
        };
        let p = Parser::new();
        let (p, f) = p.rcstext(input);
        assert_eq!(f, IResult::Done(empty, expected));
    }
}
